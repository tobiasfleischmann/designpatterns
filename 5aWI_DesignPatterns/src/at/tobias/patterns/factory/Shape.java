package at.tobias.patterns.factory;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Shape {
	public void render(GameContainer gc, Graphics graphics);
	public void move();
}
