package at.tobias.patterns.factory;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class PatternMain extends BasicGame{
	
	private List <Shape> shapes;
	
	public PatternMain() {
		super("Factory Pattern");
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		 for (Shape shapes : this.shapes){
	            shapes.render(gc, graphics);
	          }
		
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		this.shapes = new ArrayList<>();
		
		ShapeFactory shapeFactory = new ShapeFactory();
		
		Shape shape1 = shapeFactory.getShape("CIRCLE");
		
		Shape shape2 = shapeFactory.getShape("RECTANGLE");

		
		this.shapes.add(shape1);
		this.shapes.add(shape2);
		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for (Shape shapes : this.shapes){
            shapes.move();
          }
		
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new PatternMain());
			container.setDisplayMode(1000,1000,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	
}
