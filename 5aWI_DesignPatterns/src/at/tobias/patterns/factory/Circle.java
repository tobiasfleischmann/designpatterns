package at.tobias.patterns.factory;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Circle implements Shape{

	public Circle() {
		this.render(gc, graphics);
	}

	@Override
	public void move() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) {
		graphics.drawOval(500, 500, 100, 100);
		
	}
}
