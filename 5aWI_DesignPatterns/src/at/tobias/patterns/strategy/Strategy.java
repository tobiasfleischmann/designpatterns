package at.tobias.patterns.strategy;

public interface Strategy {
	public void move(int delta);
	public float getX();
	public float getY();
}
