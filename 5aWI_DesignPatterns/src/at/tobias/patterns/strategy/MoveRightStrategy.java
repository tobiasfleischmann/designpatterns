package at.tobias.patterns.strategy;

public class MoveRightStrategy implements Strategy{
	private float x, y;
	
	public MoveRightStrategy(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public void move(int delta) {
		this.x++;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

}
