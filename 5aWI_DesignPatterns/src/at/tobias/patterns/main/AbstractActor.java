package at.tobias.patterns.main;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import at.tobias.patterns.strategy.Strategy;

public abstract class AbstractActor implements Actor {
	protected Strategy strategy;
	
	public AbstractActor(Strategy strategy) {
		this.strategy = strategy;
	}
	
	public void move() {
		this.strategy.move(0);
	}
	
	@Override
	public void render(GameContainer gc, Graphics graphics) {
		// TODO Auto-generated method stub
	}
}
