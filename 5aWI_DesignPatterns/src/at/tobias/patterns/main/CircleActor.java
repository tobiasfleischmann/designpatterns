package at.tobias.patterns.main;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.tobias.patterns.observer.Observable;
import at.tobias.patterns.observer.Observer;
import at.tobias.patterns.strategy.MoveLeftStrategy;
import at.tobias.patterns.strategy.Strategy;

public class CircleActor extends AbstractActor implements Observable{
	
	private MoveLeftStrategy strategy;
	
	public CircleActor(Strategy strategy) {
		super(strategy);
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) {
		graphics.drawOval(this.strategy.getX(), this.strategy.getY(), 100, 100);
	}

	@Override
	public void addObserver(Observer observer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void informAll() {
		// TODO Auto-generated method stub
		
	}
}	
