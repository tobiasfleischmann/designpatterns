package at.tobias.patterns.main;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import at.tobias.patterns.strategy.MoveLeftStrategy;
import at.tobias.patterns.strategy.MoveRightStrategy;

public class PatternMain extends BasicGame{

	private List <Actor> actors;
	
	public PatternMain() {
		super("Patterns");
	}


	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		 for (Actor actor : this.actors){
	            actor.render(gc, graphics);
	          }
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		this.actors = new ArrayList<>();
		
		MoveRightStrategy mrs1 = new MoveRightStrategy(100,100);
		MoveRightStrategy mrs2 = new MoveRightStrategy(150,150);
		MoveLeftStrategy mls1 = new MoveLeftStrategy(150,150);
		
		this.actors.add(new RectActor(mrs1));
	    this.actors.add(new RectActor(mrs2));
	    this.actors.add(new CircleActor(mls1));

		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for (Actor actor : this.actors){
            actor.move();
          }

	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new PatternMain());
			container.setDisplayMode(1000,1000,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
}
