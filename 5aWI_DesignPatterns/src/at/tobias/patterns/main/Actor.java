package at.tobias.patterns.main;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Actor {
	public void render(GameContainer gc, Graphics graphics);
	public void move();
}
