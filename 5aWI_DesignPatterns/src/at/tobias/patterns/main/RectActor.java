package at.tobias.patterns.main;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.tobias.patterns.observer.Observer;
import at.tobias.patterns.strategy.MoveRightStrategy;

public class RectActor extends AbstractActor implements Observer{
	
	private MoveRightStrategy strategy;

	public RectActor(MoveRightStrategy strategy) {
		super(strategy);
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) {
		graphics.drawRect(this.strategy.getX(), this.strategy.getY(), 100, 100);
	}

	@Override
	public void inform() {
		// TODO Auto-generated method stub
		
	}
}
