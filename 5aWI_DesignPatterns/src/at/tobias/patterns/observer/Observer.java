package at.tobias.patterns.observer;

public interface Observer {
	public void inform();
}
