package at.tobias.patterns.observer;

public interface Observable {
	public void addObserver(Observer observer);
	public void informAll();
}
