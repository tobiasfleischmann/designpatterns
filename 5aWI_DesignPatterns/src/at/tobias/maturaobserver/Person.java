package at.tobias.maturaobserver;

public interface Person {
	public String getName();
}
