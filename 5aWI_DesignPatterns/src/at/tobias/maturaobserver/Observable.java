package at.tobias.maturaobserver;

import at.tobias.patterns.observer.Observer;

public interface Observable {
	public void addObserver(Observer observer);
	public void informAll();
}
