package at.tobias.maturaobserver;

public interface Observer {
	public void inform();
}
