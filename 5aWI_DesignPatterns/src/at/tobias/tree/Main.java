package at.tobias.tree;

public class Main {
	
	public static void main(String[] args) {
		SuperGrow sg1 = new SuperGrow();
		
		ConiferousTree ct = new ConiferousTree(sg1);
		
		System.out.println(ct.getMaxHeight());
		System.out.println(ct.getMaxDiameter());
		System.out.println(ct.getName());
	}
}
