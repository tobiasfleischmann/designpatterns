package at.tobias.tree;

public interface Fertilizer {
	public int getAmount();
}
