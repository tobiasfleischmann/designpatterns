package at.tobias.tree;

public class ConiferousTree extends AbstractTree {
	
	public int MaxHeight = 20;
	public int MaxDiameter = 2;
	public String Name = "Coniferous Tree";
	
	public ConiferousTree(Fertilizer fertilizer) {
		super(fertilizer);
	}
	
	@Override
	public int getMaxHeight() {
		return MaxHeight;
	}
	@Override
	public int getMaxDiameter() {
		return MaxDiameter;
	}

	@Override
	public String getName() {
		return Name;
	}
	
	
	

}
