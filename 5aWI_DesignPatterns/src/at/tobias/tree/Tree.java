package at.tobias.tree;

public interface Tree {
	public int getMaxHeight();
	public int getMaxDiameter();
	public String getName();
}
