package at.tobias.tree;

public abstract class AbstractTree implements Tree{
	 protected Fertilizer fertilizer;
	 
	 public AbstractTree(Fertilizer fertilizer) {
		 this.fertilizer = fertilizer;
	 }
}
